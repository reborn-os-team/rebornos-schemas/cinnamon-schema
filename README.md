# Cinnamon schema

**CINNAMON SCHEMAS**

See in: https://github.com/linuxmint/cinnamon-desktop/tree/master/schemas


**Name of file**:

95_cinnamon.gschema.override

The file **95_cinnamon.gschema.override** is used in the ISO (cnchi Gnome based)



**File locate at**:

```
/usr/share/glib-2.0/schemas/95_cinnamon.gschema.override
```

To compile schema:

```
/usr/bin/glib-compile-schemas /usr/share/glib-2.0/schemas
```


See examples:

*************************************************************
```
[org.cinnamon.desktop.interface]
keyboard-layout-show-flags=true
gtk-theme = 'Flat-Plat-Blue-light-compact'
icon-theme = 'Flat-Remix'
font-name = 'Sans 9'
cursor-theme = 'Adwaita'
clock-use-24h=true
clock-show-date=false

[org.cinnamon.desktop.wm.preferences]
theme = 'Flat-Plat-Blue-light-compact'

[org.cinnamon.desktop.background]
picture-uri = 'file:///usr/share/backgrounds/gnome/Fabric.jpg'

[org.cinnamon.settings-daemon.peripherals.touchpad]
vertical-two-finger-scrolling=true
horizontal-two-finger-scrolling=true
tap-to-click=true
touchpad-enabled=true

[org.cinnamon]
enabled-applets=['panel1:left:1:menu@cinnamon.org:0', 'panel1:left:2:panel-launchers@cinnamon.org:1', 'panel1:left:3:window-list@cinnamon.org:2', 'panel1:right:0:removable-drives@cinnamon.org:3', 'panel1:right:1:keyboard@cinnamon.org:4', 'panel1:right:2:bluetooth@cinnamon.org:5', 'panel1:right:4:systray@cinnamon.org:7', 'panel1:right:5:power@cinnamon.org:8', 'panel1:right:6:network@cinnamon.org:6', 'panel1:right:7:sound@cinnamon.org:9', 'panel1:right:8:calendar@cinnamon.org:10', 'panel1:right:9:settings@cinnamon.org:11']
favorite-apps=['nemo.desktop', 'chromium.desktop', 'pamac.desktop', 'gnome-terminal.desktop']
keyboard-applet-use-flags=false
panels-enabled=['1:0:bottom']
next-applet-id=12

[org.gnome.software]
download-updates=false
show-nonfree-software=true
allow-updates=false
show-ratings=true
```

